# -*- coding: utf-8 -*-

import os
import codecs
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with codecs.open(os.path.join(here, 'README.md')) as f:
    README = f.read()
with codecs.open(os.path.join(here, 'CHANGELOG.md')) as f:
    CHANGES = f.read()

requires = [
    'shapely',
    'numpy'
]

setup(
    name='lidar_colorizer',
    version='1.0.0',
    description='Offers handy tools for colorization of LAS files via orthographic images',
    long_description=u'{readme}\n\n{changes}'.format(readme=README, changes=CHANGES),
    classifiers=[
        "Development Status :: 4 - Alpha",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3.6",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application"
    ],
    license=u'BSD',
    author=u'Clemens Rudert',
    author_email=u'clemens.rudert@bl.ch',
    url=u'https://gitlab.com/gf-bl/lidar_colorizer',
    keywords='lidar LAS LAZ colorization geotiff projection',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    entry_points={
        'console_scripts': [
            'pdal_0_repair_srs = lidar_colorizer.lib.pdal.repair_srs:main',
            'pdal_1_create_match_for_colorize = lidar_colorizer.lib.pdal.match:main',
            'pdal_2_colorize_from_match = lidar_colorizer.lib.pdal.colorize:main',
            'tools_create_dop_grid = lidar_colorizer.lib.tools.create_dop_grid:main',
            'tools_create_las_grid = lidar_colorizer.lib.tools.create_las_grid:main',
            'tools_create_match_grid = lidar_colorizer.lib.tools.create_match_grid:main'
        ]
    }
)

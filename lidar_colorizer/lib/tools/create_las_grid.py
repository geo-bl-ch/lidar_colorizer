# -*- coding: utf-8 -*-
import glob
import json
import optparse
import os
import subprocess
import logging
import sys
from multiprocessing.pool import ThreadPool

from shapely.geometry import Polygon, mapping
from lidar_colorizer import path_leaf

log = logging.getLogger()
log.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

count = None
pointer = 1
geo_json_dict = {
    "type": "FeatureCollection",
    "features": []
}


def multi_run_wrapper_las(args):
    return work_las(*args)


def work_las(las_file):
    global pointer, count, geo_json_dict
    log.info('{0} - {1}/{2}'.format(path_leaf(las_file), pointer, count))
    pipe = subprocess.Popen(
        ['pdal', 'info', '--metadata', las_file],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    std_out, std_error = pipe.communicate()
    if std_error:
        log.error(std_error)
    content_dict = json.loads(std_out)
    metadata_dict = content_dict['metadata']
    top = round(metadata_dict['maxx'] + metadata_dict['offset_x'], 3)
    right = round(metadata_dict['maxy'] + metadata_dict['offset_y'], 3)
    bottom = round(metadata_dict['minx'] + metadata_dict['offset_x'], 3)
    left = round(metadata_dict['miny'] + metadata_dict['offset_y'], 3)
    las_polygon = Polygon([
        (top, left), (top, right), (bottom, right), (bottom, left), (top, left)
    ])
    feature = {
        'properties': {
            'name': path_leaf(las_file)
        },
        'geometry': mapping(las_polygon)
    }
    geo_json_dict["features"].append(feature)
    pointer += 1


def run(las_path, geo_json_path=None, crs="EPSG:2056", lidar_postfix='las', threads=1, name=None):
    global geo_json_dict, pointer, count
    las_files = glob.glob('{dir}/*.{postfix}'.format(dir=las_path, postfix=lidar_postfix))
    count = len(las_files)
    log.info('Start of processing {} LIDAR files'.format(count))
    geo_json_dict["crs"] = {
        "type": "name",
        "properties": {
            "name": crs
        }
    }

    if name:
        geo_json_dict["name"] = name
    if not name and las_path == './':
        log.warning('Name of dataset will be "./". Please provide a name or provide a valid folder.')
        geo_json_dict["name"] = las_path
    else:
        geo_json_dict["name"] = os.path.dirname(las_path)

    if threads > 1:
        parameters = []
        pool = ThreadPool(threads)
        for dop_file_name in las_files:
            parameters.append((dop_file_name,))
        pool.map(multi_run_wrapper_las, parameters)
    else:
        for dop_file_name in las_files:
            work_las(dop_file_name)

    if not geo_json_path:
        geo_json_path = las_path

    geo_json_file = open(os.path.join(geo_json_path, 'grid.json'), 'w+')
    geo_json_file.write(json.dumps(geo_json_dict, indent=2))
    geo_json_file.close()
    log.info('Finished processing of {0} files.'.format(count))


def main():
    parser = optparse.OptionParser(
        usage='usage: %prog [options]',
        description='Creates a 2D grid representing all LIDAR tiles by their BBOXes and stores it as a '
                    'geopackage. Every tile will have its name stored as attribute.'
    )
    parser.add_option(
        '-l', '--las-path',
        dest='las_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where the LAS files will be looked up.'
    )
    parser.add_option(
        '-g', '--geo-json-path',
        dest='geo_json_path',
        metavar='DIRECTORY',
        default=None,
        type='string',
        help='The path where the resulting geojson file will be stored in. If not provided the las path '
             'will be used.'
    )
    parser.add_option(
        '-L', '--lidar-postfix',
        dest='lidar_postfix',
        metavar='STRING',
        type='str',
        default='las',
        help='The type aka the ending of the LIDAR files to be used in the defined folders. '
             'Default is "las" files'
    )
    parser.add_option(
        '-e', '--epsg-code',
        dest='srs',
        metavar='NUMBER',
        type='str',
        default="EPSG:2056",
        help='The EPSG code like "EPSG:2056". This example '
             'will set vertical projection to LV95 swiss horizontal system. Default is "EPSG:2056"'
    )
    parser.add_option(
        '-j', '--jobs',
        dest='jobs',
        metavar='NUMBER',
        type='int',
        default=1,
        help='Number of parallel jobs to use for crawling through the sets to find a match. Default is 1'
    )
    parser.add_option(
        '-n', '--name',
        dest='name',
        metavar='DATASET NAME',
        type='str',
        help='The name which will be assigned to feature collection in geojson file for more easy loading in '
             'GIS applications.'
    )
    options, args = parser.parse_args()

    run(
        options.las_path,
        options.geo_json_path,
        options.srs,
        options.lidar_postfix,
        options.jobs,
        options.name
    )

# -*- coding: utf-8 -*-
import glob
import json
import logging
import optparse
import os
import subprocess
import sys
from multiprocessing.pool import ThreadPool

from shapely.geometry import Polygon, mapping
from lidar_colorizer import path_leaf

log = logging.getLogger()
log.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

count = None
pointer = 1
geo_json_dict = {
    "type": "FeatureCollection",
    "features": []
}


def multi_run_wrapper_las(args):
    return work_dop(*args)


def work_dop(dop_file):
    global pointer, count, geo_json_dict
    log.info('{0} - {1}/{2}'.format(path_leaf(dop_file), pointer, count))
    pipe = subprocess.Popen(
        ['gdalinfo', '-json', dop_file],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    std_out, std_error = pipe.communicate()
    if std_error:
        log.error(std_error)
    content_dict = json.loads(std_out)
    dop_polygon = Polygon([
        tuple(content_dict['cornerCoordinates']['upperLeft']),
        tuple(content_dict['cornerCoordinates']['upperRight']),
        tuple(content_dict['cornerCoordinates']['lowerRight']),
        tuple(content_dict['cornerCoordinates']['lowerLeft']),
        tuple(content_dict['cornerCoordinates']['upperLeft']),
    ])
    feature = {
        'properties': {
            'name': path_leaf(dop_file)
        },
        'geometry': mapping(dop_polygon)
    }
    geo_json_dict["features"].append(feature)
    pointer += 1


def run(dop_path, geo_json_path=None, crs="EPSG:2056", precision=3, image_postfix='tif', threads=1,
        name=None):
    global geo_json_dict, pointer, count
    dop_files = glob.glob('{dir}/*.{postfix}'.format(dir=dop_path, postfix=image_postfix))
    count = len(dop_files)
    log.info('Start of processing {} LIDAR files'.format(count))
    geo_json_dict["crs"] = {
        "type": "name",
        "properties": {
            "name": crs
        }
    }

    if name:
        geo_json_dict["name"] = name
    if not name and dop_path == './':
        log.warning('Name of dataset will be "./". Please provide a name or provide a valid folder.')
        geo_json_dict["name"] = dop_path
    else:
        geo_json_dict["name"] = os.path.dirname(dop_path)

    if threads > 1:
        parameters = []
        pool = ThreadPool(threads)
        for dop_file_name in dop_files:
            parameters.append((dop_file_name,))
        pool.map(multi_run_wrapper_las, parameters)
    else:
        for dop_file_name in dop_files:
            work_dop(dop_file_name)

    if not geo_json_path:
        geo_json_path = dop_path

    geo_json_file = open(os.path.join(geo_json_path, 'grid.json'), 'w+')
    geo_json_file.write(json.dumps(geo_json_dict, indent=2))
    geo_json_file.close()


def main():
    parser = optparse.OptionParser(
        usage='usage: %prog [options]',
        description='Creates a 2D grid representing all ortho photos by their BBOXes and stores it as a '
                    'geopackage. Every tile will have its name stored as attribute.'
    )
    parser.add_option(
        '-d', '--dop-path',
        dest='dop_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where the DOP files will be looked up.'
    )
    parser.add_option(
        '-g', '--geo-json-path',
        dest='geo_json_path',
        metavar='DIRECTORY',
        type='string',
        default=None,
        help='The path where the resulting geojson file will be stored in. If not provided the las path '
             'will be used.'
    )
    parser.add_option(
        '-D', '--image_postfix',
        dest='image_postfix',
        metavar='STRING',
        type='str',
        default='tif',
        help='The type aka the ending of the image files to be used in the defined folders. '
             'Default is "tif" files'
    )
    parser.add_option(
        '-p', '--precision',
        dest='precision',
        metavar='NUMBER',
        type='int',
        default=3,
        help='Precision which is used for matching. Default value is 3 which means 0.000 or millimeters in a '
             'metric coordinate system. It is only necessary to change this if your LIDAR data and ortho '
             'photos have a huge difference of precision. E.g. ortho resolution 20m and LIDAR 1mm.'
    )
    parser.add_option(
        '-e', '--epsg-code',
        dest='srs',
        metavar='NUMBER',
        type='str',
        default="EPSG:2056",
        help='The EPSG code like "EPSG:2056". This example '
             'will set vertical projection to LV95 swiss horizontal system. Default is "EPSG:2056"'
    )
    parser.add_option(
        '-j', '--jobs',
        dest='jobs',
        metavar='NUMBER',
        type='int',
        default=1,
        help='Number of parallel jobs to use for crawling through the sets to find a match. Default is 1'
    )
    parser.add_option(
        '-n', '--name',
        dest='name',
        metavar='DATASET NAME',
        type='str',
        help='The name which will be assigned to feature collection in geojson file for more easy loading in '
             'GIS applications.'
    )
    options, args = parser.parse_args()

    run(
        options.dop_path,
        options.geo_json_path,
        options.srs,
        options.precision,
        options.image_postfix,
        options.jobs,
        options.name
    )

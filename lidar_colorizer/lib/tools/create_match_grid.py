# -*- coding: utf-8 -*-
import json
import optparse
import os
from shapely.geometry import mapping
from shapely.wkt import loads


def run(geo_json_path, match_path, crs="EPSG:2056"):
    match_file = open(match_path, 'r')
    match_content = json.loads(match_file.read())
    match_file.close()
    geo_json_dict = {
        "crs": {
            "type": "name",
            "properties": {
                "name": crs
            }
        },
        "type": "FeatureCollection",
        "features": []
    }
    for las_match in match_content['hits']:
        las_polygon = loads(las_match['wkt'])
        if len(las_match['dops']) == 0:
            hit_type = 'no_hit'
        elif len(las_match['dops']) == 1:
            hit_type = 'single_hit'
        elif len(las_match['dops']) > 1:
            hit_type = 'multiple_hits'
        else:
            hit_type = 'unknown'
        feature = {
            'properties': {
                'name': las_match['las'],
                'type': hit_type
            },
            'geometry': mapping(las_polygon)
        }
        geo_json_dict["features"].append(feature)
    geo_json_file = open(os.path.join(geo_json_path, 'las_grid.json'), 'w+')
    geo_json_file.write(json.dumps(geo_json_dict, indent=2))
    geo_json_file.close()


def main():
    parser = optparse.OptionParser(
        usage='usage: %prog [options]',
        description='Creates a 2D grid of ortho photos and LIDAR tiles read from match file.'
    )
    parser.add_option(
        '-g', '--geo-json-path',
        dest='geo_json_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where the resulting geojson file will be stored in.'
    )
    parser.add_option(
        '-m', '--match-path',
        dest='match_path',
        metavar='ABSOLUTE FILE PATH',
        type='string',
        help='The path to the match file file.'
    )
    parser.add_option(
        '-e', '--epsg-code',
        dest='srs',
        metavar='NUMBER',
        type='str',
        default="EPSG:2056",
        help='The EPSG code like "EPSG:2056". This example '
             'will set vertical projection to LV95 swiss horizontal system. Default is "EPSG:2056"'
    )
    options, args = parser.parse_args()

    run(
        options.geo_json_path,
        options.match_path,
        options.srs
    )

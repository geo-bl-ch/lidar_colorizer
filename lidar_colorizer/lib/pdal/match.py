# -*- coding: utf-8 -*-
import json
import optparse
import glob
import os
import logging
import sys
import subprocess

from multiprocessing.pool import ThreadPool
from shapely.geometry import Polygon

from lidar_colorizer import path_leaf

log = logging.getLogger()
log.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

to_merge = {
    'las_path': None,
    'dop_path': None,
    'hits': []
}
dop_bound_polygons = []
las_pointer = 1
dop_pointer = 1


def multi_run_wrapper_las(args):
    return work_las(*args)


def work_las(las_files_count, las_file_name, dop_polygons):
    global to_merge, las_pointer
    pipe = subprocess.Popen(
        ['pdal', 'info', '--summary', las_file_name],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    std_out, std_error = pipe.communicate()
    content_dict = json.loads(std_out)
    metadata_dict = content_dict['metadata']
    top = round(metadata_dict['maxx'] + metadata_dict['offset_x'], 3)
    right = round(metadata_dict['maxy'] + metadata_dict['offset_y'], 3)
    bottom = round(metadata_dict['minx'] + metadata_dict['offset_x'], 3)
    left = round(metadata_dict['miny'] + metadata_dict['offset_y'], 3)
    las_polygon = Polygon([
        (top, left), (top, right), (bottom, right), (bottom, left), (top, left)
    ])
    matching_necessary = {
        'las': path_leaf(las_file_name),
        'wkt': las_polygon.wkt,
        'dops': []
    }
    for dop_polygon in dop_polygons:
        if las_polygon.relate_pattern(dop_polygon['polygon'], '2********'):
            matching_necessary.get('dops').append(
                {'name': dop_polygon['name'], 'wkt': dop_polygon['polygon'].wkt}
            )
    to_merge['hits'].append(matching_necessary)
    log.info(
        '{0}/{1} found number of dops: {2}'.format(
            las_pointer,
            las_files_count,
            len(matching_necessary['dops'])
        )
    )
    las_pointer += 1


def multi_run_wrapper_dop(args):
    return work_dop(*args)


def work_dop(dop_files_count, dop_file_name, precision):
    global dop_bound_polygons, dop_pointer
    log.info('Processing DOP {0}/{1}'.format(dop_pointer, dop_files_count))
    dop_pointer += 1
    pipe = subprocess.Popen(
        ['gdalinfo', '-json', dop_file_name],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )
    std_out, std_error = pipe.communicate()
    if std_error:
        log.error(std_error)
    content_dict = json.loads(std_out)
    dop_bound_polygons.append({
        'name': path_leaf(dop_file_name),
        'polygon': Polygon([
            tuple(content_dict['cornerCoordinates']['upperLeft']),
            tuple(content_dict['cornerCoordinates']['upperRight']),
            tuple(content_dict['cornerCoordinates']['lowerRight']),
            tuple(content_dict['cornerCoordinates']['lowerLeft']),
            tuple(content_dict['cornerCoordinates']['upperLeft']),
        ])
    })


def match(target_path, las_path, dop_path, threads=1, precision=3, image_postfix='tif', lidar_postfix='las'):
    las_files = glob.glob('{dir}/*.{postfix}'.format(dir=las_path, postfix=lidar_postfix))
    dop_files = glob.glob('{dir}/*.{postfix}'.format(dir=dop_path, postfix=image_postfix))
    to_merge['dop_path'] = dop_path
    to_merge['las_path'] = las_path
    to_merge_path = os.path.join(target_path, 'merge.json')
    to_merge_file = open(to_merge_path, 'w+')
    dop_count = len(dop_files)
    if threads > 1:
        parameters = []
        pool = ThreadPool(threads)
        for dop_index, dop_file_name in enumerate(dop_files):
            parameters.append((dop_count, dop_file_name, precision))
        pool.map(multi_run_wrapper_dop, parameters)
    else:
        for dop_index, dop_file_name in enumerate(dop_files):
            work_dop(dop_count, dop_file_name, precision)

    las_count = len(las_files)
    if threads > 1:
        parameters = []
        pool = ThreadPool(threads)
        for las_index, las_file_name in enumerate(las_files):
            parameters.append((las_count, las_file_name, dop_bound_polygons))
        pool.map(multi_run_wrapper_las, parameters)
    else:
        for las_index, las_file_name in enumerate(las_files):
            work_las(las_count, las_file_name, dop_bound_polygons)
    to_merge_file.writelines(json.dumps(to_merge, indent=4))
    to_merge_file.close()
    log.info('total LAS files handled: {}'.format(las_pointer-1))


def main():
    parser = optparse.OptionParser(
        usage='usage: %prog [options]',
        description='Tool to find quickly matching tiles of independent LIDAR and ortho photo sets and store '
                    'this matching in a JSON structure for later colorization via pdal. '
    )
    parser.add_option(
        '-t', '--target-path',
        dest='target_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where resulting pipelines will be stored.'
    )
    parser.add_option(
        '-l', '--las-path',
        dest='las_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where the LAS files will be looked up.'
    )
    parser.add_option(
        '-L', '--lidar-postfix',
        dest='lidar_postfix',
        metavar='STRING',
        type='str',
        default='las',
        help='The type aka the ending of the LIDAR files to be used in the defined folders. '
             'Default is "las" files'
    )
    parser.add_option(
        '-d', '--dop-path',
        dest='dop_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where the digital ortho photos will be looked up.'
    )

    parser.add_option(
        '-D', '--image_postfix',
        dest='image_postfix',
        metavar='STRING',
        type='str',
        default='tif',
        help='The type aka the ending of the image files to be used in the defined folders. '
             'Default is "tif" files'
    )
    parser.add_option(
        '-j', '--jobs',
        dest='jobs',
        metavar='NUMBER',
        type='int',
        default=1,
        help='Number of parallel jobs to use for crawling through the sets to find a match. Default is 1'
    )
    parser.add_option(
        '-p', '--precision',
        dest='precision',
        metavar='NUMBER',
        type='int',
        help='Precision which is used for matching. Default value is 3 which means 0.000 or millimeters in a '
             'metric coordinate system. It is only necessary to change this if your LIDAR data and ortho '
             'photos have a huge difference of precision. E.g. ortho resolution 20m and LIDAR 1mm.'
    )
    options, args = parser.parse_args()

    match(
        options.target_path,
        options.las_path,
        options.dop_path,
        options.jobs,
        options.precision,
        options.image_postfix,
        options.lidar_postfix
    )

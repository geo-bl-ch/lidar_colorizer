# -*- coding: utf-8 -*-
import json
import logging
import optparse
import os
import subprocess
import sys
from multiprocessing.pool import ThreadPool

from lidar_colorizer import path_leaf

log = logging.getLogger()
log.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

count = None
pointer = 1


def multi_run_wrapper_las(args):
    return work_las(*args)


def work_las(las_file, dop_file, las_colored_path, output_lidar_postfix, dop_file_index=None):
    global pointer, count
    input_file_name = path_leaf(las_file)
    output_file_name = input_file_name.replace(input_file_name.split('.')[-1], output_lidar_postfix)
    if dop_file_index:
        output_file_name = '{0}_{1}'.format(dop_file_index, output_file_name)

    output_file_path = os.path.join(las_colored_path, output_file_name)
    log.info('{0} - {1}/{2}'.format(input_file_name, pointer, count))
    pipe = subprocess.Popen(
        [
            'pdal',
            'pipeline',
            '-s'
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        stdin=subprocess.PIPE,
        encoding='utf8'
    )
    std_out, std_error = pipe.communicate(
        input=json.dumps([
            {
                "type": "readers.las",
                "filename": las_file
            },
            {
                "type": "filters.colorization",
                "raster": dop_file
            },
            {
                "type": "writers.las",
                "minor_version": "2",
                "dataformat_id": "3",
                "filename": output_file_path
            }
        ])
    )
    if std_out:
        log.info(std_out)
    if std_error:
        log.error(std_error)
    pointer += 1


def run(match_file_path, las_colored_directory, output_lidar_postfix='laz', threads=1):
    global pointer, count
    match_file = open(match_file_path)
    colorize_dict = json.loads(match_file.read())
    match_file.close()
    count = len(colorize_dict['hits'])
    log.info('Start of processing {} LIDAR files'.format(count))
    las_path = colorize_dict['las_path']
    dop_path = colorize_dict['dop_path']
    if threads > 1:
        parameters = []
        pool = ThreadPool(threads)
        for las_file in colorize_dict['hits']:
            matched_dops = len(las_file['dops'])
            for index, dop in enumerate(las_file['dops']):
                if matched_dops == 1:
                    parameters.append((
                        os.path.join(las_path, las_file['las']),
                        os.path.join(dop_path, dop['name']),
                        las_colored_directory,
                        output_lidar_postfix
                    ))
                    pool.map(multi_run_wrapper_las, parameters)
                elif matched_dops > 1:
                    parameters.append((
                        os.path.join(las_path, las_file['las']),
                        os.path.join(dop_path, dop['name']),
                        las_colored_directory,
                        output_lidar_postfix,
                        index
                    ))
                    pool.map(multi_run_wrapper_las, parameters)
    else:
        for las_file in colorize_dict['hits']:
            matched_dops = len(las_file['dops'])
            for index, dop in enumerate(las_file['dops']):
                if matched_dops == 1:
                    work_las(
                        os.path.join(las_path, las_file['las']),
                        os.path.join(dop_path, dop['name']),
                        las_colored_directory,
                        output_lidar_postfix
                    )
                elif matched_dops > 1:
                    work_las(
                        os.path.join(las_path, las_file['las']),
                        os.path.join(dop_path, dop['name']),
                        las_colored_directory,
                        output_lidar_postfix,
                        dop_file_index=index
                    )
    log.info('Finished processing of {0} files.'.format(count))


def main():
    parser = optparse.OptionParser(
        usage='usage: %prog [options]',
        description='Tool for creating pdal pipelines for colorization of LAS data with ortho photos from a '
                    'previously matched set of data.'
    )
    parser.add_option(
        '-m', '--match-file',
        dest='match_file',
        metavar='FILE',
        type='string',
        help='The path where match file is situated.'
    )
    parser.add_option(
        '-l', '--las-colored-path',
        dest='las_colored_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where the colored LAS files will be stored in.'
    )
    parser.add_option(
        '-o', '--output-lidar-postfix',
        dest='output_lidar_postfix',
        metavar='STRING',
        type='str',
        default='las',
        help='The type aka the ending of the LIDAR files to be used for storing repaired files. '
             'Default is "laz" files (compressed)'
    )
    parser.add_option(
        '-j', '--jobs',
        dest='jobs',
        metavar='NUMBER',
        type='int',
        default=1,
        help='Number of parallel jobs to use for crawling through the sets to find a match. Default is 1'
    )
    options, args = parser.parse_args()

    run(
        options.match_file,
        options.las_colored_path,
        options.output_lidar_postfix,
        options.jobs
    )

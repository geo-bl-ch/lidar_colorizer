# -*- coding: utf-8 -*-
import glob
import json
import logging
import optparse
import os
import subprocess
import sys
from multiprocessing.pool import ThreadPool

from lidar_colorizer import path_leaf

log = logging.getLogger()
log.setLevel(logging.INFO)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(message)s')
handler.setFormatter(formatter)
log.addHandler(handler)

count = None
pointer = 1


def multi_run_wrapper_las(args):
    return work_las(*args)


def work_las(las_file, las_repaired_path, input_lidar_postfix, output_lidar_postfix, crs):
    global pointer, count
    input_file_name = path_leaf(las_file)
    output_file_name = input_file_name.replace(input_lidar_postfix, output_lidar_postfix)
    output_file_path = os.path.join(las_repaired_path, output_file_name)
    log.info('{0} - {1}/{2}'.format(input_file_name, pointer, count))
    pipe = subprocess.Popen(
        [
            'pdal',
            'pipeline',
            '-s'
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        stdin=subprocess.PIPE,
        encoding='utf8'
    )
    std_out, std_error = pipe.communicate(
        input=json.dumps([
            {
                "type": "readers.las",
                "filename": las_file
            },
            {
                "type": "writers.las",
                "a_srs": crs,
                "filename": output_file_path
            }
        ])
    )
    if std_out:
        log.info(std_out)
    if std_error:
        log.error(std_error)
    pointer += 1


def run(las_path, las_repaired_path, crs="EPSG:2056+5729", input_lidar_postfix='las',
        output_lidar_postfix='laz', threads=1):
    global pointer, count
    las_files = glob.glob('{dir}/*.{postfix}'.format(dir=las_path, postfix=input_lidar_postfix))
    count = len(las_files)
    log.info('Start of processing {} LIDAR files'.format(count))
    if threads > 1:
        parameters = []
        pool = ThreadPool(threads)
        for las_file in las_files:
            parameters.append((
                las_file,
                las_repaired_path,
                input_lidar_postfix,
                output_lidar_postfix,
                crs
            ))
        pool.map(multi_run_wrapper_las, parameters)
    else:
        for las_file in las_files:
            work_las(
                las_file,
                las_repaired_path,
                input_lidar_postfix,
                output_lidar_postfix,
                crs
            )
    log.info('Finished processing of {0} files.'.format(count))


def main():
    parser = optparse.OptionParser(
        usage='usage: %prog [options]',
        description='Tool for creating pdal pipelines to repair broken SRS information. This tool does not '
                    'transform anything. It only assigns the defined SRS information via EPSG definitions. '
                    'Use case is a wrongly set SRS in delivered files while you know what SRS it should be. '
                    'This tool should be used carefully since it can mix up your data sustainably.'
    )
    parser.add_option(
        '-l', '--las-path',
        dest='las_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where the LAS files will be looked up.'
    )
    parser.add_option(
        '-r', '--las-repaired-path',
        dest='las_repaired_path',
        metavar='DIRECTORY',
        type='string',
        help='The path where the repaired LAS files will be stored in.'
    )
    parser.add_option(
        '-i', '--input-lidar-postfix',
        dest='input_lidar_postfix',
        metavar='STRING',
        type='str',
        default='las',
        help='The type aka the ending of the LIDAR files to be used in the defined folders. '
             'Default is "las" files'
    )
    parser.add_option(
        '-o', '--output-lidar-postfix',
        dest='output_lidar_postfix',
        metavar='STRING',
        type='str',
        default='las',
        help='The type aka the ending of the LIDAR files to be used for storing repaired files. '
             'Default is "laz" files (compressed)'
    )
    parser.add_option(
        '-e', '--epsg-code',
        dest='srs',
        metavar='NUMBER',
        type='str',
        default="EPSG:2056+5729",
        help='The EPSG code which should contain information about horizontal AND the vertical projection. '
             'You should put it in a PROJ4/pdal consumable way like "EPSG:2056+5729". This example will set '
             'vertical projection to LV95 swiss horizontal system and to LHN95 swiss vertical system.'
    )
    parser.add_option(
        '-j', '--jobs',
        dest='jobs',
        metavar='NUMBER',
        type='int',
        default=1,
        help='Number of parallel jobs to use for crawling through the sets to find a match. Default is 1'
    )
    options, args = parser.parse_args()

    run(
        options.las_path,
        options.las_repaired_path,
        options.srs,
        options.input_lidar_postfix,
        options.output_lidar_postfix,
        options.jobs
    )

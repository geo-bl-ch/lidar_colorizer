# Check if running on CI
ifeq ($(CI),true)
  PIP_REQUIREMENTS = requirements-timestamp
  VENV_BIN =
  PYTHON =
else
  PIP_REQUIREMENTS = .venv/requirements-timestamp
  VENV_BIN = .venv/bin
  PYTHON = .venv/.timestamp
endif

# Set pip and setuptools versions
PIP_VERSION ?= pip==19.2.1
SETUPTOOL_VERSION ?= setuptools==41.0.1

# Build dependencies
BUILD_DEPS += .venv/requirements-timestamp

# Package name
PACKAGE = lidar_colorizer

# Python source files
SRC_PY = $(shell find $(PACKAGE) -name '*.py')

# *******************
# Set up environments
# *******************

.venv/timestamp:
	python3 -m venv .venv --system-site-packages
	$(VENV_BIN)/pip3 install '$(PIP_VERSION)' '$(SETUPTOOL_VERSION)'
	touch $@

.venv/requirements-timestamp: setup.py requirements.txt
	$(VENV_BIN)/pip3 install -r requirements.txt
	touch $@

dist/timestamp:
	$(VENV_BIN)/python setup.py bdist_egg
	touch $@

.PHONY: install
install: .venv/timestamp .venv/requirements-timestamp

.PHONY: build
build: install dist/timestamp
	$(VENV_BIN)/python setup.py install

.PHONY: deploy-pypi
deploy-pypi: install build

.PHONY: clean
clean:
	rm -rf .pytest_cache/
	rm -rf build/
	rm -rf dist/
	rm -f .coverage

.PHONY: clean-all
clean-all: clean
	rm -rf .venv
	rm -rf *.egg-info

.PHONY: git-attributes
git-attributes:
	git --no-pager diff --check `git log --oneline | tail -1 | cut --fields=1 --delimiter=' '`

.PHONY: lint
lint: $(PIP_REQUIREMENTS) setup.cfg $(SRC_PY)
	$(VENV_BIN)/flake8

.PHONY: test
test: $(PIP_REQUIREMENTS) $(SRC_PY)
	$(VENV_BIN)/py.test -vv --cov-config .coveragerc --cov $(PACKAGE) --cov-report term-missing:skip-covered \
		test

FROM registry.gitlab.com/geo-bl-ch/docker/pdal:v2.0.1

COPY --chown=1001:0 . /app/

USER root

#
# Install lidar colorizer app
#
RUN echo "http://dl-8.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk --update add \
        libc-dev \
        gcc && \
    apk --update add --virtual .lidar-colorizer-deps \
        make \
        g++ \
        libpng-dev \
        freetype-dev \
        python3-dev \
        py3-numpy-dev \
        file && \
    cd /app/ && \
    make build && \
    apk del .lidar-colorizer-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

RUN chown 1001 /data && \
    chgrp 0 /data && \
    chmod g=u /data && \
    chown 1001 /app && \
    chown --recursive 1001 /app && \
    chgrp 0 /app && \
    chmod g=u /app && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd

CMD ["echo /app/.venv/bin/pdal_1_create_match_for_colorize -h"]

USER 1001

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

WORKDIR /data

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["pdal_1_create_match_for_colorize -h"]

lidar_colorizer is a collection of usefull scripts to handle LIDAR data. It utilizes pdal and gdal and other
famous libraries as well. In the moment it offers convenience methods to deal with a big amount of files.

You can use this package in a environment with pdal and gdal installed. But since this might be a pain to
achieve a Dockerfile is offered. You can extend the behaviour with it and make your own image or you might
directly use the prepared images from the projects registry. Find more information about docker usage in the
docker section of this readme.

The library is intended to be used as command line tool. It offers 3 tools to handle LIDAR related work for
colorizing.

In addition it offers 3 easy to use tools to create an overview of BBOXES of ortho photo tiles,
LIDAR tiles and an overview of BBOXES of the matching between LIDAR and ortho photos. Output is the
GeoJSON format. This is very handy if you simply want to generate the web suitable tile structure.

LIDAR COLORIZATION
==================

Colorization is done in 1 + 2 steps. First step is not mandatory if the SRS in LAS files is correct:

0. (pdal_0_repair_srs)
1. pdal_1_create_match_for_colorize
2. pdal_2_colorize_from_match

pdal_0_repair_srs
-----------------

In order to be able to correctly colorize the LIDAR data from ortho photos it's the best to have them in the
same coordinate system. It often happens that LIDAR data is not in correct coordinate system at all regarding
to meta information but the contained points are correct. So this tool try to solve this issue. It assigns
new and correct coordinate system into the meta data. It does **NOT** reproject the data. So before you use
this tool be absolutely sure what your meta data says and what the actually point data says. The doc can be
obtained by:

`pdal_0_repair_srs --h`

pdal_1_create_match_for_colorize
--------------------------------

Handling many files while colorization end up in an iterative matching of LAS and image files. Having
thousands of such will end up with iteration over all image tiles for each LAS tile. This is not very usefull
and very time consuming. The tool *pdal_1_create_match_for_colorize* tries to solve this problem by saving
all the matches in a JSON matching file. The creation process can be multi threaded and therefore massively
speed up. The doc can be obtained by:

`pdal_1_create_match_for_colorize --h`

pdal_2_colorize_from_match
--------------------------

Easy as you would think this tool takes information out of the match file and iterates over the matches to
assign the colorisation. This step also offers multi threading. The doc can be obtained by:

`pdal_2_colorize_from_match --h`

It is important to know that through one process following the steps 0-2 the path of files should not change.
Otherwise the process won't work.

Auxiliaries
===========

Handling big amount of tiles its always handy to have a a visualisation of what you are dealing with. So this
tools are your rescue.

tools_create_dop_grid
---------------------

Simply take a folder of ortho photos and puts out a GeoJSON representing the tiles BBOX's with file name as
attribute per BBOX. The doc can be obtained by:

`tools_create_dop_grid --h`

tools_create_las_grid
---------------------

Simply take a folder of LAS tiles and puts out a GeoJSON representing the tiles BBOX's with file name as
attribute per BBOX. The doc can be obtained by:

`tools_create_las_grid --h`

tools_create_match_grid
-----------------------

This tool can be used after step *pdal_1_create_match_for_colorize*. It gives a visual representation of the
matching in GeoJSON format. Filenames are attached as attributes to each BBOX. In Addition it shows which LAS
has matched which ortho photo. The doc can be obtained by:

`tools_create_match_grid --h`

Docker usage
============

To use docker for this package simply run it like this:

`docker run --rm -v <local_path_to_data>:/data/ registry.gitlab.com/geo-bl-ch/lidar_colorizer:latest \
pdal_1_create_match_for_colorize -t <target_path> -l <las_path> -L laz -d <ortho_path> -j 4`
